package logaday_logrus

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/iamtyler/logaday.go"
)

type Hook struct {
	logger    *logaday.Logger
	formatter logrus.Formatter
}

func NewHook(logger *logaday.Logger, formatter logrus.Formatter) (*Hook, error) {
	return &Hook{
		logger:    logger,
		formatter: formatter,
	}, nil
}

func (h *Hook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (h *Hook) Fire(entry *logrus.Entry) error {
	bytes, err := h.formatter.Format(entry)
	if err != nil {
		return err
	}

	writer, err := h.logger.GetWriter(entry.Time)
	if err != nil {
		return err
	}
	count, err := writer.Write(bytes)
	if err != nil {
		return err
	}
	if count != len(bytes) {
		return fmt.Errorf("wrote only %d of %d bytes", count, len(bytes))
	}

	return nil
}
