package logaday_logrus

import (
	"bytes"
	"io/ioutil"
	"os"
	"reflect"
	"runtime"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/iamtyler/logaday.go"
)

const (
	testDir    = "logs"
	testPath   = "logs/test.00010101.log"
	testFormat = "logs/test.{date}.log"

	testMessage = "test"
	testLine    = "time=\"0001-01-01T00:01:00Z\" level=info msg=test\n"
)

var (
	testTime = time.Date(1, 1, 1, 0, 1, 0, 0, time.UTC)
)

func cleanWorkspace(t *testing.T) {
	err := os.RemoveAll(testDir)
	if err != nil {
		t.Fatalf("Error cleaning workspace: %s", err)
	}
}

func requireFileContents(t *testing.T, path string, data []byte) {
	fileData, err := ioutil.ReadFile(path)
	if os.IsNotExist(err) {
		t.Fatalf("File required %s", path)
	} else if err != nil {
		t.Fatalf("Error checking file %s: %s", path, err)
	}

	if !bytes.Equal(data, fileData) {
		t.Log("File contents mismatch:")
		t.Logf("  Expected: %v", data)
		t.Logf("  Actual:   %v", fileData)
	}
}

func testHook(t *testing.T) *Hook {
	logger, err := logaday.NewLogger(testFormat)
	if err != nil {
		t.Fatalf("error creating logger: %s", err)
		return nil
	}

	hook, err := NewHook(logger, &logrus.TextFormatter{})
	if err != nil {
		t.Fatalf("error creating hook: %s", err)
		return nil
	}

	return hook
}

func Test_NewHook(t *testing.T) {
	testHook(t)
}

func Test_Levels(t *testing.T) {
	hook := testHook(t)

	levels := hook.Levels()
	if !reflect.DeepEqual(levels, logrus.AllLevels) {
		t.Errorf("hook did not return all levels: %v", levels)
	}
}

func Test_Fire(t *testing.T) {
	defer runtime.GC()
	defer cleanWorkspace(t)

	cleanWorkspace(t)

	hook := testHook(t)

	err := hook.Fire(&logrus.Entry{
		Time:    testTime,
		Level:   logrus.InfoLevel,
		Message: testMessage,
	})
	if err != nil {
		t.Errorf("fire error: %s", err)
	}

	requireFileContents(t, testPath, []byte(testLine))
}
