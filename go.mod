module gitlab.com/iamtyler/logaday-logrus.go

require (
	github.com/sirupsen/logrus v1.0.6
	gitlab.com/iamtyler/logaday.go v1.0.0
	golang.org/x/crypto v0.0.0-20180820150726-614d502a4dac // indirect
	golang.org/x/sys v0.0.0-20180824143301-4910a1d54f87 // indirect
)
